/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/06 17:49:56 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:28:34 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s1)
{
	size_t	start;
	size_t	end;
	size_t	counter;
	char	*result;

	start = 0;
	if (!s1)
		return (NULL);
	while (s1[start] && (s1[start] == ' ' || s1[start] == '\n' ||
		s1[start] == '\t'))
		start++;
	if (start > ft_strlen(s1) - 1)
		return (ft_strnew(1) ? ft_strnew(1) : NULL);
	end = ft_strlen(s1) - 1;
	while (s1[end] && (s1[end] == ' ' || s1[end] == '\n' ||
		s1[end] == '\t'))
		end--;
	counter = 0;
	if (!(result = ft_strnew(end - start + 1)))
		return (NULL);
	end = end - start + 1;
	while (counter < end)
		result[counter++] = s1[start++];
	return (result);
}
