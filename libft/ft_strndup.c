/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 21:58:49 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/29 22:10:19 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, size_t n)
{
	size_t	i;
	char	*result;

	i = 0;
	if(!(result = ft_strnew(ft_strlen(s1))))
		return (NULL);
	while (s1[i] && i < n)
	{
		result[i] = s1[i];
		i++;
	}
	return (result);
}
