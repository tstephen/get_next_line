/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 07:25:53 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/24 15:58:40 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t dstsize)
{
	size_t		src_len;
	size_t		i;
	size_t		j;

	src_len = ft_strlen(src);
	i = 0;
	j = 0;
	while (*dest && i < dstsize)
	{
		dest++;
		i++;
	}
	if (i < dstsize)
	{
		while (*src && j < dstsize - i - 1)
		{
			*dest = *src;
			dest++;
			src++;
			j++;
		}
		*dest = '\0';
	}
	return (src_len + i);
}
