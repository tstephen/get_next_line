/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/18 15:57:25 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/24 09:42:52 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		nb;
	int		sign;

	nb = 0;
	sign = 1;
	while (((*str > 8 && *str < 14) || *str == 32) && *str)
		str++;
	if (*str == '-')
	{
		sign = -1;
		str++;
	}
	if (*str == '+' && sign != -1)
		str++;
	while (*str && (*str >= '0' && *str <= '9'))
	{
		nb *= 10;
		nb += *str - '0';
		str++;
	}
	return (nb * sign);
}
