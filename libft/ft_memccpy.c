/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 13:33:57 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:19:17 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned	char	*ptr_dst;
	unsigned	char	*ptr_src;
	size_t				i;
	unsigned	char	chr;

	i = 0;
	chr = (unsigned char)c;
	ptr_dst = (unsigned char*)dst;
	ptr_src = (unsigned char*)src;
	while (i < n)
	{
		*ptr_dst = *ptr_src;
		i++;
		ptr_dst++;
		if (*ptr_src == chr)
			return (ptr_dst);
		ptr_src++;
	}
	return (NULL);
}
