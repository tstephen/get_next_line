/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/25 00:18:24 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 08:37:28 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*current;
	t_list	*new;
	t_list	*head;

	if (!lst)
		return (NULL);
	new = (*f)(lst);
	current = new;
	head = new;
	lst = lst->next;
	while (lst)
	{
		new = (*f)(lst);
		current->next = new;
		current = current->next;
		lst = lst->next;
	}
	return (head);
}
