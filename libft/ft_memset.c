/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 12:09:38 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:18:14 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	chr;
	unsigned char	*ptr;
	size_t			i;

	chr = (unsigned char)c;
	ptr = (unsigned char*)b;
	i = 0;
	while (i < len)
	{
		*ptr = chr;
		ptr++;
		i++;
	}
	return (b);
}
