/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/12 17:21:55 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/19 19:03:36 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	int	counter;

	counter = 0;
	while (*src)
	{
		dest[counter] = *src;
		counter++;
		src++;
	}
	dest[counter] = '\0';
	return (dest);
}
