/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/23 19:52:30 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:44:38 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_find_length(int n)
{
	int counter;

	counter = 0;
	if (n == 0)
	{
		return (counter + 1);
	}
	while (n != 0)
	{
		counter++;
		n /= 10;
	}
	return (counter);
}

static char	*ft_build_arr(int n, int sign, int length)
{
	int		counter;
	char	*result;

	counter = length - 1;
	if (n == 0)
	{
		if (!(result = ft_strnew(1)))
			return (NULL);
		*result = '0';
		return (result);
	}
	if (!(result = (char*)malloc(sizeof(*result) * length + 1)))
		return (NULL);
	if (sign == -1)
		result[0] = '-';
	while (n != 0)
	{
		if (n > 0)
			result[counter] = n % 10 + '0';
		else
			result[counter] = -1 * (n % 10) + '0';
		counter--;
		n /= 10;
	}
	return (result);
}

char		*ft_itoa(int n)
{
	int		sign;
	int		length;
	char	*result;

	sign = 1;
	if (n < 0)
		sign = -1;
	if (sign == -1)
		length = ft_find_length(n) + 1;
	else
		length = ft_find_length(n);
	if (!(result = ft_build_arr(n, sign, length)))
		return (NULL);
	result[length] = '\0';
	return (result);
}
