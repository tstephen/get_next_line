/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/06 17:25:39 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:28:07 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		len1;
	int		len2;
	char	*result;
	int		counter;

	if (!s1 || !s2)
		return (NULL);
	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	if (!(result = ft_strnew(len1 + len2 + 1)))
		return (NULL);
	counter = 0;
	while (*s1)
	{
		result[counter] = *s1;
		s1++;
		counter++;
	}
	while (*s2)
	{
		result[counter] = *s2;
		s2++;
		counter++;
	}
	return (result);
}
