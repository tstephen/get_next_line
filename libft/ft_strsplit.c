/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/08 17:01:49 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:29:29 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_find_size(char const *s, char c)
{
	size_t	size;
	size_t	i;

	size = 0;
	i = 0;
	while (s[i] != '\0')
	{
		while (s[i] == c)
			i++;
		if (s[i])
		{
			size++;
			while (s[i] && s[i] != c)
				i++;
		}
	}
	return (size);
}

static size_t	ft_get_index(char const *s, char c, size_t start)
{
	while (s[start] && s[start] == c)
	{
		start += 1;
	}
	return (start);
}

static size_t	ft_substrlen(char const *s, char c, size_t start)
{
	size_t len;

	len = 0;
	while (s[start] != c)
	{
		len++;
		start++;
	}
	return (len);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t		size;
	char		**result;
	size_t		strlen;
	size_t		i;
	size_t		index;

	if (!s)
		return (NULL);
	size = ft_find_size(s, c);
	i = 0;
	index = 0;
	if (!(result = (char**)malloc(sizeof(char**) * size + 1)))
		return (NULL);
	while (i < size)
	{
		index = ft_get_index(s, c, index);
		strlen = ft_substrlen(s, c, index);
		if (!(result[i] = ft_strnew(strlen)))
			return (NULL);
		result[i] = ft_strsub(s, index, strlen);
		index += strlen;
		i++;
	}
	result[i] = NULL;
	return (result);
}
