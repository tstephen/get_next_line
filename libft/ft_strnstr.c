/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 21:27:24 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:23:58 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_check_len(const char *str, const char *sub, size_t i,
				size_t len)
{
	size_t		counter;

	counter = i;
	while (*sub && str[i] && counter < len)
	{
		if (str[i] != *sub)
			return (0);
		i++;
		counter++;
		sub++;
	}
	if (*sub)
		return (0);
	return (1);
}

char			*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t		i;
	int			check;
	char		*found;

	i = 0;
	if (!(*little))
		return ((char*)big);
	while (big[i] && i < len)
	{
		if (big[i] == little[0])
		{
			check = ft_check_len(big, little, i, len);
			if (check)
			{
				found = (char*)(big + i);
				return (found);
			}
		}
		i++;
	}
	return (NULL);
}
