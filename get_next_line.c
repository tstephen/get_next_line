/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 12:35:25 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/03 15:09:19 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	ft_make1_res(char **res, char **buf, int *index)
{
	char	*ptr_res;
	char	*temp_res;

	*index = ft_strichr(*buf, '\n', ++(*index));
	if (*index == -1 && !(*res))
		*res = ft_strdup(*buf);
	else if (*index == -1 && *res)
	{
		ptr_res = *res;
		*res = ft_strjoin(*res, *buf);
		free(ptr_res);
	}
	else if (*index != -1 && !(*res))
		*res = ft_strsub(*buf, 0, *index);
	else if (*index != -1 && *res)
	{
		ptr_res = *res;
		temp_res = ft_strsub(*buf, 0, *index - 1 == -1 ? 0 : *index);
		*res = ft_strjoin(*res, temp_res);
		free(ptr_res);
		free(temp_res);
	}
}

char	*ft_first_read(const int fd, char **buf, int *index, int *size)
{
	char	*res;

	res = NULL;
	while ((*size = read(fd, *buf, BUFF_SIZE)) && *size != -1)
	{
		if (size > 0)
		{
			(*buf)[*size] = '\0';
			ft_make1_res(&res, buf, index);
			if (*index != -1)
				return (res);
		}
	}
	if (!res)
		return (NULL);
	else
		return (res);
}

void	ft_make2_res(char **res, char **buf, int *index)
{
	int		temp;
	char	*ptr_res;
	char	*substr;

	temp = *index;
	*index = ft_strichr(*buf, '\n', ++(*index));
	if (*index == -1 && !(*res))
		*res = ft_strdup(&(*buf)[temp + 1]);
	else if (*index != -1 && !(*res))
		*res = ft_strndup(&(*buf)[temp + 1], *index - temp - 1);
	else if (*index == -1 && *res)
	{
		ptr_res = *res;
		*res = ft_strjoin(*res, &(*buf)[temp + 1]);
		free(ptr_res);
	}
	else if (*index != -1 && *res)
	{
		ptr_res = *res;
		substr = ft_strsub(&(*buf)[temp + 1], 0, *index - temp - 1);
		*res = ft_strjoin(*res, substr);
		free(substr);
		free(ptr_res);
	}
}

char	*ft_sec_read(const int fd, char **buf, int *index, int *size)
{
	char	*res;

	res = NULL;
	while (*size > 0)
	{
		ft_make2_res(&res, buf, index);
		if (*index != -1)
			return (res);
		*size = read(fd, *buf, BUFF_SIZE);
		(*buf)[*size] = '\0';
	}
	if (!res)
		return (NULL);
	else
		return (res);
}

int		get_next_line(const int fd, char **line)
{
	static	char	buffer[BUFF_SIZE + 1];
	static	int		index = -1;
	int				size;
	char			*buf;

	if (fd < 0 || !(line))
		return (-1);
	size = 1;
	buf = buffer;
	*line = NULL;
	if (index == -1)
		*line = ft_first_read(fd, &buf, &index, &size);
	else
		*line = ft_sec_read(fd, &buf, &index, &size);
	if (size == -1)
		return (-1);
	else if ((*line))
	{
		if (ft_strlen(*line) > 0 || index != -1)
			return (1);
		return (0);
	}
	else
		return (0);
}