# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/27 10:04:56 by tstephen          #+#    #+#              #
#    Updated: 2018/06/03 13:00:56 by tstephen         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = main.c

all:
	@gcc -Wall -Wextra -Werror $(NAME) get_next_line.c libft.a

clean:
	@/bin/rm -rf a.out

re: clean all

lib:
	@cd libft && make again
	@cp -f ./libft/libft.a .
