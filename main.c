/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 20:43:28 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/03 12:58:16 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		main(void)
{
	int			fd;
	int			ret;
	char	*line1;
	char	*line2;
	char	*line3;

	fd = open("./test04.txt", O_RDONLY);
	if (fd == -1)
	{
		ft_putendl("Opening file failed");
		close(fd);
	}
	ft_putendl("--------------FIRST CALL------------");
	ret = get_next_line(fd, &line1);
	ft_putendl(line1);
	ft_putnbr(ret);
	ft_putchar('\n');
	ft_putendl("------------------------------------");
	ft_putendl("-------------SECOND CALL------------");
	ret = get_next_line(fd, &line2);
	ft_putendl(line2);
	ft_putnbr(ret);
	ft_putchar('\n');
	ft_putendl("------------------------------------");
	ft_putendl("--------------THIRD CALL------------");
	ret = get_next_line(fd, &line3);
	ft_putendl(line3);
	ft_putnbr(ret);
	ft_putchar('\n');
	ft_putendl("------------------------------------");
	ft_putendl("--------------FOURTH CALL------------");
	free(line1);
	ret = get_next_line(fd, &line1);
	ft_putendl(line1);
	ft_putnbr(ret);
	ft_putchar('\n');
	ft_putendl("------------------------------------");
	close(fd);

/*	fd = open("./test04.txt", O_RDWR | O_CREAT);
	write(fd, "abc\n\n", 5);
	int	i = get_next_line(fd, &line1);
	ft_putendl(line1);
	ft_putnbr(i);
	ft_putchar('\n');*/
	return (0);
}
